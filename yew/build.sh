# cargo install wasm-bindgen-cli
cargo clean
cargo build --release --target wasm32-unknown-unknown
~/.cargo/bin/wasm-bindgen --target web --out-dir ../static --out-name wasm target/wasm32-unknown-unknown/release/rust_1_yew.wasm --no-typescript