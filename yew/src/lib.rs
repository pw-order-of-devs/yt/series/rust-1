#![recursion_limit="256"]
#[macro_use] extern crate yew;

use wasm_bindgen::prelude::*;
use yew::prelude::*;

pub struct App {
    title: String,
}

pub enum Msg {
    SetTitle(String),
    DoSomething,
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        App {
            title: "App title".to_string(),
        }
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn rendered(&mut self, first_render: bool) {
        if first_render {
            // do something
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::SetTitle(t) => self.title = t,
            Msg::DoSomething => println!("doing something"),
        }
        true
    }

    fn view(&self) -> Html {
        html! {
            <div>
                <p>{ &self.title }</p>
                <p>{ "Hello world!" }</p>
            </div>
        }
    }
}

#[wasm_bindgen]
pub fn run_app() -> Result<(), JsValue> {
    yew::start_app::<App>();
    Ok(())
}