#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Account {
    pub username: String,
    pub password: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct JWTClaims {
    pub exp: i64,
    pub iat: i64,
    pub iss: String,
}