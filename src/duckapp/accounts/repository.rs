use r2d2::Pool;
use r2d2_mysql::MysqlConnectionManager;
use r2d2_mysql::mysql::prelude::Queryable;
use std::sync::Arc;

use easy_hasher::easy_hasher::sha256;
use crate::duckapp::accounts::domain::Account;

fn account_mapper(params: (String, String)) -> Account {
    let (username, password) = params;
    Account { username, password }
}

fn to_sha256(input: String) -> String {
    sha256(&input).to_hex_string().to_lowercase()
}

pub fn get_by_username_and_password(
    db_pool: &Arc<Pool<MysqlConnectionManager>>,
    username: String, password: String
) -> Option<Account> {
    let pool = db_pool.clone();
    let mut conn = pool.get().unwrap();
    conn.exec_first(
        "select username, password from rust1.accounts where username = ? and password = ?",
        (username, to_sha256(password),)
    ).unwrap().map(account_mapper)
}
