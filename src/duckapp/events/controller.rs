use std::sync::Arc;

use actix_web::{HttpRequest, HttpResponse};
use actix_web::web::{Data, Json, Path};
use r2d2::Pool;
use r2d2_mysql::MysqlConnectionManager;

use crate::duckapp::events::domain::EventRequest;
use crate::duckapp::events::repository;

pub async fn get_all(req: HttpRequest) -> HttpResponse {
    let db_pool = req.app_data::<Arc<Pool<MysqlConnectionManager>>>().unwrap();
    HttpResponse::Ok().json(repository::get_all(db_pool.get().unwrap()))
}

pub async fn get_by_id(db_pool: Data<Pool<MysqlConnectionManager>>, _req: HttpRequest, path: Path<u64>) -> HttpResponse {
    HttpResponse::Ok().json(repository::get_by_id(db_pool.get().unwrap(), path.into_inner()))
}

pub async fn create(db_pool: Data<Pool<MysqlConnectionManager>>, _req: HttpRequest, json: Json<EventRequest>) -> HttpResponse {
    HttpResponse::Ok().json(repository::create(db_pool.get().unwrap(), json.into_inner()))
}

pub async fn update(db_pool: Data<Pool<MysqlConnectionManager>>, _req: HttpRequest, path: Path<u64>, json: Json<EventRequest>) -> HttpResponse {
    HttpResponse::Ok().json(repository::update(db_pool.get().unwrap(), path.into_inner(), json.into_inner()))
}

pub async fn delete(db_pool: Data<Pool<MysqlConnectionManager>>, _req: HttpRequest, path: Path<u64>) -> HttpResponse {
    HttpResponse::Ok().json(repository::delete(db_pool.get().unwrap(), path.into_inner()))
}