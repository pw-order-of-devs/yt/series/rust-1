use chrono::NaiveDateTime;
use juniper::{GraphQLInputObject, GraphQLObject};

#[derive(Serialize, Deserialize, Clone, Debug, GraphQLObject)]
pub struct Event {
    pub id: String,
    pub name: String,
    pub description: String,
    pub date: NaiveDateTime,
}

#[derive(Serialize, Deserialize, Clone, Debug, GraphQLInputObject)]
pub struct EventRequest {
    pub name: String,
    pub description: String,
    pub date: NaiveDateTime,
}