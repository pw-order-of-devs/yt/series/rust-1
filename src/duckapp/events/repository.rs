use chrono::NaiveDateTime;
use r2d2::PooledConnection;
use r2d2_mysql::MysqlConnectionManager;
use r2d2_mysql::mysql::prelude::Queryable;

use crate::duckapp::events::domain::{Event, EventRequest};

fn event_mapper(params: (u64, String, String, NaiveDateTime)) -> Event {
    let (id, name, description, date) = params;
    Event { id: id.to_string(), name, description, date }
}

pub fn get_all(db_pool: PooledConnection<MysqlConnectionManager>) -> Vec<Event> {
    let mut conn = db_pool;
    conn.query_map("select id, name, description, date from rust1.events", event_mapper).unwrap()
}

pub fn get_by_id(db_pool: PooledConnection<MysqlConnectionManager>, id: u64) -> Event {
    let mut conn = db_pool;
    conn.exec_first(
        "select id, name, description, date from rust1.events where id = ?", (id,)
    ).unwrap().map(event_mapper).unwrap()
}

pub fn create(db_pool: PooledConnection<MysqlConnectionManager>, event: EventRequest) -> () {
    let mut conn = db_pool;
    conn.exec_drop(
        "insert into rust1.events (name, description, date) values (?, ?, ?)",
        (event.name, event.description, event.date,)
    ).unwrap()
}

pub fn update(db_pool: PooledConnection<MysqlConnectionManager>, id: u64, event: EventRequest) -> () {
    let mut conn = db_pool;
    conn.exec_drop(
        "update rust1.events set name = ?, description = ?, date = ? where id = ?",
        (event.name, event.description, event.date, id,)
    ).unwrap()
}

pub fn delete(db_pool: PooledConnection<MysqlConnectionManager>, id: u64) -> () {
    let mut conn = db_pool;
    conn.exec_drop(
        "delete from rust1.events where id = ?",
        (id,)
    ).unwrap()
}