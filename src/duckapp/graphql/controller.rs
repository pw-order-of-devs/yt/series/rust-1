use std::sync::Arc;
use actix_web::{web, Error};
use actix_web::web::HttpResponse;
use juniper::http::GraphQLRequest;
use r2d2::Pool;
use r2d2_mysql::MysqlConnectionManager;

use crate::duckapp::graphql::schema::{Schema, Context};

pub async fn graphql(
    pool: web::Data<Pool<MysqlConnectionManager>>,
    st: web::Data<Arc<Schema>>,
    data: web::Json<GraphQLRequest>,
) -> Result<HttpResponse, Error> {
    let event = web::block(move || {
        let res = data.execute(&st, &Context { dbpool: pool.get_ref().to_owned(), });
        Ok::<_, serde_json::error::Error>(serde_json::to_string(&res)?)
    }).await.map_err(Error::from)?;
    Ok(HttpResponse::Ok()
        .content_type("application/json")
        .body(event))
}