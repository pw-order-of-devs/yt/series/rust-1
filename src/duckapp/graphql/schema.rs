use juniper::{RootNode, FieldResult};

use r2d2::Pool;
use r2d2_mysql::MysqlConnectionManager;

use crate::duckapp::events::domain::{Event, EventRequest};
use crate::duckapp::events::repository;

pub struct Context {
    pub dbpool: Pool<MysqlConnectionManager>,
}

impl juniper::Context for Context {}

pub struct QueryRoot;

#[juniper::object(Context = Context)]
impl QueryRoot {
    #[graphql(description = "List of all event")]
    fn events(context: &Context) -> FieldResult<Vec<Event>> {
        Ok(repository::get_all(context.dbpool.get().unwrap()))
    }
    #[graphql(description = "Event by id")]
    fn event(context: &Context, id: String) -> FieldResult<Event> {
        Ok(repository::get_by_id(context.dbpool.get().unwrap(), id.parse::<u64>().unwrap()))
    }
}

pub struct MutationRoot;

#[juniper::object(Context = Context)]
impl MutationRoot {
    fn create_event(context: &Context, new_event: EventRequest) -> FieldResult<String> {
        repository::create(context.dbpool.get().unwrap(), new_event);
        Ok("Ok".to_owned())
    }
    fn update_event(context: &Context, new_event: EventRequest, id: String) -> FieldResult<String> {
        repository::update(context.dbpool.get().unwrap(), id.parse::<u64>().unwrap(), new_event);
        Ok("Ok".to_owned())
    }
    fn delete_event(context: &Context, id: String) -> FieldResult<String> {
        repository::delete(context.dbpool.get().unwrap(), id.parse::<u64>().unwrap());
        Ok("Ok".to_owned())
    }
}

pub type Schema = RootNode<'static, QueryRoot, MutationRoot>;

pub fn create_schema() -> Schema {
    Schema::new(QueryRoot {}, MutationRoot {})
}