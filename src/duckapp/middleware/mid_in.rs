use std::task::{Context, Poll};

use futures::future::{ok, FutureExt, LocalBoxFuture, Ready};

use actix_web::http::header::{HeaderName, HeaderValue};
use actix_web::dev::{ServiceRequest, ServiceResponse, Transform, Service};
use actix_web::Error;

#[derive(Clone)]
pub struct MidIn;

impl Default for MidIn {
    fn default() -> Self {
        MidIn {}
    }
}

impl MidIn {
    pub fn new() -> MidIn {
        MidIn::default()
    }
}

impl<B, S> Transform<S> for MidIn
    where
        S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
        S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Transform = MidInMiddleware<S>;
    type InitError = ();
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(MidInMiddleware {
            service
        })
    }
}

pub struct MidInMiddleware<S> {
    service: S,
}

impl <S, B> Service for MidInMiddleware<S>
    where
        S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
        S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Future = LocalBoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.service.poll_ready(cx)
    }

    #[allow(clippy::borrow_interior_mutable_const)]
    fn call(&mut self, mut req: ServiceRequest) -> Self::Future {
        info!("Request on path {} started", req.path());
        req.headers_mut().insert(HeaderName::from_static("x-new-header-in"), HeaderValue::from_static("value"));
        let fut = self.service.call(req);
        async move {
            Ok(fut.await?)
        }.boxed_local()
    }
}