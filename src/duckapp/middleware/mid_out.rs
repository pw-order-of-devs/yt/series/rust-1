use std::task::{Context, Poll};

use futures::future::{ok, FutureExt, LocalBoxFuture, Ready};

use actix_web::http::header::{HeaderName, HeaderValue};
use actix_web::dev::{ServiceRequest, ServiceResponse, Transform, Service};
use actix_web::Error;

#[derive(Clone)]
pub struct MidOut;

impl Default for MidOut {
    fn default() -> Self {
        MidOut {}
    }
}

impl MidOut {
    pub fn new() -> MidOut {
        MidOut::default()
    }
}

impl<B, S> Transform<S> for MidOut
    where
        S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
        S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Transform = MidOutMiddleware<S>;
    type InitError = ();
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(MidOutMiddleware {
            service
        })
    }
}

pub struct MidOutMiddleware<S> {
    service: S,
}

impl <S, B> Service for MidOutMiddleware<S>
    where
        S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
        S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Future = LocalBoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.service.poll_ready(cx)
    }

    #[allow(clippy::borrow_interior_mutable_const)]
    fn call(&mut self, req: ServiceRequest) -> Self::Future {
        let fut = self.service.call(req);
        async move {
            info!("Request finished.");
            let mut res = fut.await?;
            res.headers_mut().insert(HeaderName::from_static("x-new-header-out"), HeaderValue::from_static("value"));
            Ok(res)
        }.boxed_local()
    }
}