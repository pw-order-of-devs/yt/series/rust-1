use std::borrow::BorrowMut;
use std::str::FromStr;
use std::sync::Arc;
use std::task::{Context, Poll};

use futures::future::{ok, FutureExt, LocalBoxFuture, Ready};

use actix_web::Error;
use actix_web::dev::{ServiceRequest, ServiceResponse, Transform, Service};
use actix_web::error::ErrorUnauthorized;
use actix_web::http::header::{HeaderName, HeaderValue};
use chrono::Utc;
use jsonwebtoken::Validation;
use r2d2::Pool;
use r2d2_mysql::MysqlConnectionManager;

use crate::duckapp::accounts;
use crate::duckapp::accounts::domain::JWTClaims;

#[derive(Clone)]
pub struct Auth;

impl Default for Auth {
    fn default() -> Self {
        Auth {}
    }
}

impl Auth {
    pub fn new() -> Auth {
        Auth::default()
    }
}

impl<B, S> Transform<S> for Auth
    where
        S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
        S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Transform = AuthMiddleware<S>;
    type InitError = ();
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(AuthMiddleware {
            service
        })
    }
}

pub struct AuthMiddleware<S> {
    service: S,
}

impl <S, B> Service for AuthMiddleware<S>
    where
        S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
        S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Future = LocalBoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.service.poll_ready(cx)
    }

    #[allow(clippy::borrow_interior_mutable_const)]
    fn call(&mut self, mut req: ServiceRequest) -> Self::Future {
        let auth_result = match req.borrow_mut().headers_mut().get("authorization") {
            None => None,
            Some(a) => {
                let header = a.to_str().unwrap();
                if header.starts_with("Basic") {
                    let parsed = String::from_utf8(base64::decode(header.trim_start_matches("Basic ")).unwrap()).unwrap();
                    let credentials = parsed.split(":").collect::<Vec<&str>>();

                    match accounts::repository::get_by_username_and_password(
                        req.borrow_mut().app_data::<Arc<Pool<MysqlConnectionManager>>>().unwrap(),
                        credentials[0].to_string(), credentials[1].to_string()
                    ) {
                        Some(account) => Some(jsonwebtoken::encode(
                            &jsonwebtoken::Header::default(),
                            &accounts::domain::JWTClaims {
                                exp: Utc::now().timestamp() + 3600,
                                iat: Utc::now().timestamp(),
                                iss: account.username
                            },
                            &jsonwebtoken::EncodingKey::from_secret("secret".as_ref()),
                        ).unwrap()),
                        None => None
                    }
                }
                else if header.starts_with("Bearer") {
                    match jsonwebtoken::decode::<JWTClaims>(
                        header.trim_start_matches("Bearer "),
                        &jsonwebtoken::DecodingKey::from_secret("secret".as_ref()),
                        &Validation::default(),
                    ) { Ok(_) => Some(String::from("ok")), Err(_) => None }
                }
                else { None }
            }
        };
        let fut = self.service.call(req);
        async move {
            let mut res = fut.await?;
            let headers = res.headers_mut();
            match auth_result {
                Some(token) => {
                    headers.insert(
                        HeaderName::from_str("x-auth").unwrap(),
                        HeaderValue::from_str(token.as_ref()).unwrap());
                    Ok(res)
                },
                None => Ok(res.error_response(ErrorUnauthorized("unauthorized")))
            }
        }.boxed_local()
    }
}