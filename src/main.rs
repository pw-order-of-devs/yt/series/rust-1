mod duckapp;

extern crate env_logger;
#[macro_use] extern crate log;
#[macro_use] extern crate serde_derive;

use std::sync::Arc;

use actix_web::{dev::{ Service }, guard, http::{ ContentEncoding }, middleware, web, App, HttpServer, HttpRequest, HttpResponse};
use r2d2_mysql::MysqlConnectionManager;
use r2d2_mysql::mysql::{Opts, OptsBuilder};
use openssl::ssl::{SslAcceptor, SslMethod, SslFiletype};
use wasmer::{imports, Instance, Module, Store, wat2wasm};
use wasmer_compiler_singlepass::Singlepass;
use wasmer_engine_jit::JIT;

use crate::duckapp::events;
use crate::duckapp::graphql;
use crate::duckapp::middleware as mw;

async fn version(_req: HttpRequest) -> HttpResponse {
    HttpResponse::Ok().json("0.1.0")
}

fn connection_pool() -> Arc<r2d2::Pool<MysqlConnectionManager>> {
    let url = std::env::var("DATABASE_URL").unwrap();
    let opts = Opts::from_url(&url).unwrap();
    let builder = OptsBuilder::from_opts(opts);
    let manager = MysqlConnectionManager::new(builder);
    Arc::new(r2d2::Pool::new(manager).unwrap())
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "info");
    env_logger::init();

    let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    builder.set_private_key_file("key.pem", SslFiletype::PEM).unwrap();
    builder.set_certificate_chain_file("cert.pem").unwrap();

//    let compiler = Singlepass::default();
//    let store = Store::new(&JIT::new(&compiler).engine());
//    let wasm_bytes = wat2wasm(include_bytes!("../static/wasm_bg.wasm")).unwrap();
//    let module = Module::new(&store, wasm_bytes).unwrap();
//    let instance = Instance::new(&module, &imports! {}).unwrap();
//    let run_app = instance.exports.get_function("run_app").unwrap();

    HttpServer::new(|| App::new()
        .app_data(connection_pool())
        .data(Arc::new(graphql::schema::create_schema()))
        .wrap(mw::mid_out::MidOut::new())
        .wrap(mw::mid_in::MidIn::new())
        .wrap(mw::auth::Auth::new())
        .wrap(middleware::Logger::new("%a %s %b %U %{User-Agent}i %T"))
        .wrap(middleware::DefaultHeaders::new())
        .wrap(middleware::Compress::new(ContentEncoding::Gzip))
        .service(web::resource("/graphql").route(web::post().to(graphql::controller::graphql)))
        .service(web::resource("/events")
            .guard(guard::Header("x-validation-header", "validation"))
            .guard(guard::Any(guard::Get()).or(guard::Post()))
            .route(web::get().to(events::controller::get_all))
            .route(web::post().to(events::controller::create))
        )
        .service(web::resource("/events/{id}")
            .guard(guard::Any(guard::Get()).or(guard::Put()).or(guard::Delete()))
            .route(web::get().to(events::controller::get_by_id))
            .route(web::put().to(events::controller::update))
            .route(web::delete().to(events::controller::delete))
        )
        .route("/", web::get().to(version)
            .guard(guard::All(guard::Host("localhost"))
                .and(guard::Get())
                .and(guard::fn_guard(|req| {
                    req.headers().contains_key("content-type")
                })))
        )
    ).bind_openssl("0.0.0.0:4000", builder)?.run().await
}
